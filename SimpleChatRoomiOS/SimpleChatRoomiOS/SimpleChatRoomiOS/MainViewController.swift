//
//  ViewController.swift
//  SimpleChatRoomiOS
//
//  Created by Stephen Farr on 5/12/15.
//  Copyright (c) 2015 Stephen Farr. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView?;
    @IBOutlet weak var textBox: UITextField?;
    @IBOutlet weak var sendButton: UIButton?;
    
    private var delegate: MainTableViewDelegate?;
    private var dataSource: MainTableViewDataSource?;
    
    private var timer: NSTimer?;
    private var lastRequestTime: NSDate?;
    
    private var userName: String?;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        lastRequestTime = NSDate(timeIntervalSince1970: 0);
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        delegate = MainTableViewDelegate();
        dataSource = MainTableViewDataSource();
        
        tableView?.delegate = delegate;
        tableView?.dataSource = dataSource;
        tableView?.registerNib(UINib(nibName: "NonSelfAuthoredTableViewCell", bundle: nil),
                      forCellReuseIdentifier: "NonSelfAuthoredTableViewCell");
    }
    
    override func viewDidAppear(animated: Bool) {
        var alertView: UIAlertController = UIAlertController(title:"Input Name", message:"What is your name?", preferredStyle:UIAlertControllerStyle.Alert);
        alertView.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Input name"
        }
        
        alertView.addAction(UIAlertAction(title:"Submit", style:UIAlertActionStyle.Default, handler: { (action) -> Void in
            var textField: UITextField = alertView.textFields?.first as! UITextField;
            self.userName = textField.text;
        }))
        
        self.presentViewController(alertView, animated:true) { () -> Void in
            self.queryForData();
            self.timer = NSTimer.scheduledTimerWithTimeInterval(0.5,
                target: self,
                selector: Selector("queryForData"),
                userInfo: nil,
                repeats: true);
        };
    }
    
    func queryForData() {
        var session: NSURLSession = NSURLSession.sharedSession();
        var request: NSMutableURLRequest = NSMutableURLRequest();
        
        //If the last request time is set we want just the items since the last request time otherwise request all items by passing epoch date
        request.addValue(convertDateToTimeStamp(lastRequestTime!), forHTTPHeaderField: "If-Modified-Since");
        request.URL = NSURL(string: "http://localhost:9000/message");
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData;
        
        lastRequestTime = NSDate.new();
        
        var task: NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in

            var httpResponse: NSHTTPURLResponse = response as! NSHTTPURLResponse;
            
            if (httpResponse.statusCode == 200) {
                var jsonError: NSError?;
                var jsonResponse: NSArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonError) as! NSArray!;
                
                var messageList: Array<Message> = [Message]();
                for jsonMessage in jsonResponse {
                    var message: Message = Message();
                    
                    message.messageId = jsonMessage["id"] as! String;
                    message.author = jsonMessage["author"] as! String;
                    message.body = jsonMessage["body"] as! String;
                    message.timeStamp = jsonMessage["timeStamp"] as! Int;
                    
                    messageList.append(message);
                }
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    var dataSource: MainTableViewDataSource = self.tableView?.dataSource as! MainTableViewDataSource;
                    dataSource.addMessages(messageList);
                    self.tableView?.reloadData();
                });
            }
        });
        task.resume();
    }

    @IBAction func sendTapped(sender: UIButton) {
        var message: Dictionary<String, String> = Dictionary<String, String>();
        message["body"] = textBox?.text;
        message["author"] = self.userName;
        
        var error: NSError?;
        
        var session: NSURLSession = NSURLSession.sharedSession();
        var request: NSMutableURLRequest = NSMutableURLRequest();
        request.HTTPMethod = "POST";
        request.setValue("application/json", forHTTPHeaderField: "Content-Type");
        request.URL = NSURL(string: "http://localhost:9000/message");
        
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(message, options: NSJSONWritingOptions.PrettyPrinted, error: &error);
        
        var task: NSURLSessionTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            var httpResponse: NSHTTPURLResponse = response as! NSHTTPURLResponse;
            
            if (httpResponse.statusCode == 200) {
                self.queryForData();
            }
        })
        
        task.resume();
    }
    
    func convertDateToTimeStamp(time: NSDate) -> String {
        var dateFormatter: NSDateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss.SSS";
        
        return dateFormatter.stringFromDate(time);
    }
}

