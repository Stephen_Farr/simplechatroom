//
//  Message.swift
//  SimpleChatRoomiOS
//
//  Created by Stephen Farr on 5/12/15.
//  Copyright (c) 2015 Stephen Farr. All rights reserved.
//

import UIKit

class Message: NSObject {
    internal var messageId: String = "";
    internal var author: String = "";
    internal var body: String = "";
    internal var timeStamp: Int = -1;
}
