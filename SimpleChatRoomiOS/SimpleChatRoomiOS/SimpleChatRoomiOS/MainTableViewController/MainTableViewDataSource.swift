//
//  MainTableViewControllerDataSource.swift
//  SimpleChatRoomiOS
//
//  Created by Stephen Farr on 5/12/15.
//  Copyright (c) 2015 Stephen Farr. All rights reserved.
//

import UIKit

class MainTableViewDataSource: NSObject, UITableViewDataSource {

    private var messages: Array<Message>;
    
    override init() {
        messages = [Message]();
        
        super.init();
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var message = messages[indexPath.row];
        
        var cell: NonSelfAuthoredTableViewCell = tableView.dequeueReusableCellWithIdentifier("NonSelfAuthoredTableViewCell") as! NonSelfAuthoredTableViewCell;
        cell.setMessage(message.author + ": " + message.body);
        cell.setTimeStamp(convertDateToTimeStamp((NSDate(timeIntervalSince1970:NSTimeInterval(message.timeStamp / 1000)))));
    
        return cell;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count;
    }
    
    internal func addMessages(appendMessages: Array<Message>) {
        messages += appendMessages;
    }
    
    func convertDateToTimeStamp(time: NSDate) -> String {
        var dateFormatter: NSDateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm";
        
        return dateFormatter.stringFromDate(time);
    }
}
