//
//  NonSelfAuthoredTableViewCell.swift
//  SimpleChatRoomiOS
//
//  Created by Stephen Farr on 5/13/15.
//  Copyright (c) 2015 Stephen Farr. All rights reserved.
//

import UIKit

class NonSelfAuthoredTableViewCell: UITableViewCell {

    @IBOutlet var timeStampLabel: UILabel!;
    @IBOutlet var messageContentLabel: UILabel!;
    
    internal func setTimeStamp(timeStamp: String) {
        timeStampLabel?.text = timeStamp;
    }
    
    internal func setMessage(messageContent: String) {
        messageContentLabel?.text = messageContent;
    }
    
}
