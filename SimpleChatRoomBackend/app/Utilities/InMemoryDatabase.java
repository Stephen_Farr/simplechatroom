package utilities;

import models.Message;
import org.joda.time.DateTime;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by Stephen Farr on 5/11/2015.
 */
public class InMemoryDatabase {

    private static InMemoryDatabase instance = new InMemoryDatabase();
    private ConcurrentSkipListMap<Long, Message> database;

    private InMemoryDatabase() {
        database = new ConcurrentSkipListMap<>(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Long firstObject = (Long)o1;
                Long secondObject = (Long)o2;

                return firstObject.compareTo(secondObject);
            }
        });
    }

    public static InMemoryDatabase getInstance() {
        return instance;
    }

    public void put(final Message message) {
        this.database.put(message.timeStamp, message);
    }

    public List<Message> getFromDate(final DateTime sinceDate) {
        return new LinkedList<>(this.database.tailMap(sinceDate.getMillis()).values());
    }

}
