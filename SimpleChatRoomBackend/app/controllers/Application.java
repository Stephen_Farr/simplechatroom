package controllers;

import utilities.InMemoryDatabase;
import models.Message;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.libs.Json;
import play.mvc.*;

import views.html.*;

import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result getLatestMessages() {
        String[] sinceDateArray = request().headers().get(IF_MODIFIED_SINCE);
        if (sinceDateArray == null || sinceDateArray.length < 1) {
            return badRequest();
        }

        String sinceDateString = sinceDateArray[0];

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss.SSS");
        DateTime sinceDate = formatter.parseDateTime(sinceDateString);

        List<Message> messages = InMemoryDatabase.getInstance().getFromDate(sinceDate);
        if (messages == null || messages.size() == 0) {
            return status(304, "Not Modified");
        } else {
            return ok(Json.toJson(InMemoryDatabase.getInstance().getFromDate(sinceDate)));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result postNewMessage() {
        Message message = Json.fromJson(request().body().asJson(), Message.class);
        message.timeStamp = DateTime.now(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT"))).getMillis();
        message.messageId = UUID.randomUUID().toString();

        InMemoryDatabase.getInstance().put(message);

        return ok(Json.toJson(message));
    }

}
