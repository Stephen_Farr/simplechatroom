package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Farr on 5/11/2015.
 */
public class Message {

    @JsonProperty("id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String messageId;

    @JsonProperty("author")
    public String author;

    @JsonProperty("body")
    public String body;

    @JsonProperty("timeStamp")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Long timeStamp;
}
